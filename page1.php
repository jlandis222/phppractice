<?php

/* Comment Website by J. Landis */


$sql = "SELECT * FROM j_comments;";
try
{
    $dbh = new PDO ("mysql:dbname=jacob; host=localhost",
    "Jacob", "jacob");
    $rueckgabe = $dbh->query($sql);
    $ergebnis = $rueckgabe->fetchAll(PDO::FETCH_ASSOC);
    $arrayLen = count($ergebnis); 

    $delete=$_GET['delete'];
    if(!is_null($delete))
    {
        
        $index = ($delete + 0); //converte to int
        $sql = "DELETE FROM j_comments WHERE p_id =" . $ergebnis[$index]["p_id"];
            $dbh->query($sql);
            header("Location: page1.php");
            exit;
    }
}
catch(PDOException $e)
{
    print $e->getMessage();
}
?>


<html>
<head>
<style>
head,body {
    width: 100%;
    height: 100%;
}

#main-container {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
}
#button-container {
    flex: 1 1 100px;
    margin-left: 10px;
}

#flexbox {
    display: flex;
    border-radius: 10px;
    background-color: steelblue;
    min-width: 800px;
    <?php
    if($arrayLen <= 7)
    {
        echo "height: 600px;";
    }
    else
    {
        echo "height:" . $arrayLen * 70 . "px;";
    }

    ?>
    justify-content: flex-start;
    flex-direction: column;
    padding: 20px;
    flex: 1 1 100px;
    margin-left: 50px;
}

.comment-container, .expanded-comment-container {
    background-color: lightblue;
    height: 50px;
    border-radius: 10px;
    margin: 0 0 10px 0;
    padding: 5px;
    align-self: left;
    max-height: 50px;
    display: flex;
}

.comment-container {
    width: 400px;
    max-width: 400px;
}

.expanded-comment-container {
    width: 450px;
    max-width: 450px;
}

.view-comment-container {
    background-color: lightblue;
    width: 400px;
    max-width: 400px;
    border-radius: 10px;
    margin: 0 0 10px 0;
    padding: 20px;
    align-self: left;
    max-height: 300px;
    display: flex;
}

.left-container{
    background-color: lightblue;
    flex: 1 1 400px;
    max-width: 400px;
    word-wrap: break-word;
}

.right-edge{  
    background-color: darkblue;
    flex: 1 1 10px;
    display: flex;
    flex-direction: column;
    line-height: 50px;
    text-align: center;
    align-self: left;
    margin-left: 5px;
}

.right-edge:hover {
    background-color: gold;
}

#expand-link {
    text-decoration: none;
    color: lightblue;
}

#expand-link:hover {
    text-decoration: none;
    color: white;
}

.view-button, .delete-button, .back-button {
    flex: 1 1 25px;
    background-color: darkblue;
    color: white;
    font-family: arial;
    max-height: 25px;
    min-height: 25px;
    min-width: 50px;
    text-decoration: none;
    text-align: center;
    border-radius: 10px;
}

.view-button:hover, .delete-button:hover, .back-button:hover {
    background-color: gold;
}

.view-button {
    margin-bottom: 3px;
}

.back-button {
    margin-left: 10px;
}

#newButton{
    width: 100px;
    height: 100px;
    background-color: gold;
    font-size: 400%;
    border-radius: 5px;
}

#newButton:hover{
    background-color: silver;
    color: indigo;
}


</style>
</head>
<body>
<br> <br>
<div id="main-container">
    <div id= "flexbox">

    <?php
    //Check if a comment is currently viewed
     $comment_index = $_GET["view"];
    if(isset($comment_index))
    { 
        echo "<div class=\"view-comment-container\">";

        echo "<div class=\"left-container\" style=\"max-width: 350px;\">";
        echo "PostNr." . $ergebnis[$comment_index]["p_id"] . "  " . $ergebnis[$comment_index]["c_firstname"] . " " . $ergebnis[$comment_index]["c_lastname"];
        echo "<br><br>";
        echo $ergebnis[$comment_index]["c_comment"];
        echo "</div>";

        echo "<a id=\"back-link\" href=\"?\">";
        echo "<div class=\"back-button\">Back</div></a>";
        echo "</div>";

    }
    else
    {
        for($i = $arrayLen - 1; $i >= 0; $i--)
        {
            //check if container is expanded
            if($_GET["expand"] === (string)$i)
            {
                echo "<div class=\"expanded-comment-container\">";
                echo "<div class=\"left-container\">";
                echo "PostNr." . $ergebnis[$i]["p_id"] . "  " . $ergebnis[$i]["c_firstname"] . " " . $ergebnis[$i]["c_lastname"];                
                echo "<br>";

                if(strlen($ergebnis[$i]["c_comment"]) > 50)
                {
                    echo substr($ergebnis[$i]["c_comment"],0,47) . '...';
                }  
                else{
                    echo $ergebnis[$i]["c_comment"];
                }
                echo "</div>";
                echo "<div class=\"right-container\" style=\"background-color: lightblue; line-height: 30px;\">";
                
                echo "<div class=\"expanded-button-container\">";

                echo "<a id=\"view-link\" href=\"?view=" . $i . "\">";
                echo "<div class=\"view-button\" title=\"View\">View</div></a>";

                echo "<a id=\"delete-link\" href=\"?delete=" . $i . "\">";
                echo "<div class=\"delete-button\" title=\"Delete\">Delete</div></a>";
                echo "</div>";
                echo "</div>";

                echo "<a id=\"expand-link\" href=\"?\">";
                echo  "<div class=\"right-edge\">◀</div></a>";

                echo "</div>";
            }
            else //normal container
            {
                echo "<div class=\"comment-container\">";
                echo "<div class=\"left-container\">";
                echo "PostNr." . $ergebnis[$i]["p_id"] . "  " . $ergebnis[$i]["c_firstname"] . " " . $ergebnis[$i]["c_lastname"];           
                echo "<br>";

                if(strlen($ergebnis[$i]["c_comment"]) > 50)
                {
                    echo substr($ergebnis[$i]["c_comment"],0,47) . '...';
                }  
                else{
                    echo $ergebnis[$i]["c_comment"];
                }
                
                echo "</div>";               
                echo "<a id=\"expand-link\" href=\"?expand=" . $i . "\">";
                echo "<div class=\"right-edge\">▶</div></a>";
                echo "</div>";
            }
        }
    }
    ?>

    </div>
    <div id="button-container">
        <form method="get" action="page2.php">
        <button id="newButton" type="submit" title="New Post">+</button>
        </form>
    </div>
</div>
</body>
</html>