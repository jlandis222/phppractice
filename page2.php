<?php

$nameErr = "";
$commentErr = "";
$firstname = "";
$lastname = "";
$comment = "";
$serverCon = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["firstname"])) {
    $nameErr = "Firstname is required";
  } else {   
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z-' ]*$/",$firstname)) {
      $nameErr = "Only letters and white space allowed";
    }
    else
    {
      $firstname = htmlspecialchars($_POST["firstname"]);
    }
  }
  if (empty($_POST["lastname"])) {
    $nameErr = "Lastname is required";
    } else {
    
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z-' ]*$/",$lastname)) {
       $nameErr = "Only letters and white space allowed";
    }
    else
    {
      $lastname = htmlspecialchars($_POST["lastname"]);
    }
  }
  if (empty($_POST["comment"])) {
    $commentErr = "Please enter a comment";
  } else {
    $comment = htmlspecialchars($_POST["comment"]);
  }
}

if($firstname != "" and $lastname != "" and $comment != "")
{
  $sql = "INSERT INTO j_comments (c_firstname, c_lastname, c_comment)" .
  " VALUES('$firstname', '$lastname', '$comment')";

  try
    {
      $dbh = new PDO ("mysql:dbname=Jacob; host=localhost", "Jacob", "jacob");
      $dbh->query($sql);
      $error = $dbh->errorInfo();
      if ($error[0] > 0)
    {
     print "Error: " .$error[1]."<br>".$error[2];
    }
    $dbh = null;
    }
  catch(PDOException $e)
    {
      print $e->getMessage();
    }
    header("Location: page1.php");
    exit;
}

?>

<!DOCTYPE HTML>  
<html>
<head>
<style>
h2, form {
  color: white;
}

.error {
  color: #FF0000;
}

#main-container {
  border-radius: 10px;
  background-color: steelblue;
  width: 500px;
  height: 500px;
  padding: 20px;
}

.button1 {
  width: 60px;
  margin-bottom: 10px;
  height: 50px;
  background-color: gold;
}

</style>
</head>
<body>  


<div id="main-container">
<h2> New Comment: </h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
<label for="firstname"> First Name:</label> 
 <input type="text" name="firstname" maxlenght="20" value="<?php echo $firstname;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br>
  <label for="lastname"> Last Name: </label>
  <input type="text" name="lastname" maxlenght="20" value="<?php echo $lastname;?>">
<span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  <label for="comment"> Comment: </label>
  <textarea name="comment" rows="10" cols="50" maxlength="500" style="vertical-align: top"><?php echo $comment;?></textarea>
  <span class="error">* <?php echo $commentErr;?></span>
  <br><br> 
  <button class="button1" type="submit" name="submit" title="Submit">Submit</button> 
</form>
<form method="get" action="page1.php">
  <button class="button1" type="submit" title="Back">Back</button>
</form>

<br>
<span class="server-connect"> <?php echo $serverCon;?></span>
</div>
</body>
</html>